package algorithm.search;

public class BinarySearch {
    static int binarySearch(int arr[], int start, int end, int x) {
        if (end>=start){
            int mid = start + (end - start)/2;

            if (arr[mid] == x) return mid;
            if (arr[mid] > x)  return binarySearch(arr, start, mid-1, x);

            return binarySearch(arr, mid+1, end, x);
        }
        return -1;
    }
    public static void main(String args[]) {
        int arr[] = {2,3,4,10,40};

        System.out.println(binarySearch(arr,0,arr.length-1, 11));
        System.out.println(binarySearch(arr,0,arr.length-1, 10));
    }
}
