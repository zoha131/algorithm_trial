package algorithm.dynamic;

import java.util.Scanner;

class LongestIncreasingSubsequence {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int arraySize;
        System.out.println("Enter array size: ");
        arraySize = input.nextInt();
        int seq[] = new int[arraySize];
        System.out.print("Enter elements in array: ");
        for (int i = 0; i < arraySize; i++) {
            seq[i]=input.nextInt();
        }
        longestIncreasingSubsequence(seq);
    }
    public static void longestIncreasingSubsequence(int seq[]){
        int lis[] = new int[seq.length];
        int trace[] = new int[seq.length];
        for (int i = 0; i < lis.length; i++) {
            lis[i] = 1;
        }
        for (int i = 0; i < trace.length; i++) {
            trace[i] = -1;
        }
        for (int i = 1; i < seq.length; i++) {
            for (int j = 0; j < i; j++) {
                if(seq[i]>seq[j] && lis[j]+1>lis[i]){
                    lis[i] = lis[j]+1;
                    trace[i] = j;
                }
            }
        }
        System.out.println("LIS:");
        for (int i = 0; i < lis.length; i++) {
            System.out.print(lis[i]+" ");
        }
        System.out.println("\nTrace:");
        for (int i = 0; i < trace.length; i++) {
            System.out.print(trace[i]+" ");
        }
        int max = lis[0], index=-1;
        //fiding maximum value of lis array.
        for (int i = 1; i < lis.length; i++) {
            if(max<lis[i]){
                max = lis[i];
                index = i;
            }
        }
        System.out.println("\nLength of LIS ="+max);
        System.out.println("Sequence is: ");
        traceValue(index, trace, seq);
        System.out.print(seq[index]);
    }

    public static void traceValue(int index, int trace[], int seq[]){
        if(trace[index]==-1)
            return;
        index = trace[index];
        traceValue(index, trace, seq);
        System.out.print(seq[index] + " ");

    }

}

