package algorithm.dynamic;

class LongestComSeq {
    static int getLongCOmSeq(String one, String two){
        int row = one.length() + 1;
        int col = two.length() + 1;
        int matrix[][] = new int[row][col];

        //setting 0 to all row
        for (int i = 0; i < row; i++) {
            matrix[i][0] = 0;
        }
        //setting 0 to all column
        for (int i = 0; i < col; i++) {
            matrix[0][i] = 0;
        }

        for (int i = 1; i < row; i++) {
            for (int j = 1; j < col; j++) {
                if(one.charAt(i-1) == two.charAt(j-1)){
                    matrix[i][j] = matrix[i-1][j-1] + 1;
                }
                else {
                    matrix[i][j] = Math.max(matrix[i-1][j], matrix[i][j-1]);
                }
            }
        }

        int max = matrix[row-1][col-1];
        int r = row - 1;
        int c = col - 1;

        char[] values = new char[max];
        int index = max-1;

        while(r>0 && c> 0){
            if(matrix[r][c] == matrix[r-1][c]) r--;

            else if (matrix[r][c] == matrix[r][c-1]) c--;

            else {
                values[index] = one.charAt(r-1);//two.charAt(c-1);

                index--;

                r--;
                c--;
            }
        }

        for (int i = 0; i < max; i++) {
            System.out.print(values[i]+" ");
        }
        System.out.println("");



        return max;
    }

    public static void main(String[] args) {
        System.out.println(getLongCOmSeq("abke", "abcd"));
    }
}

