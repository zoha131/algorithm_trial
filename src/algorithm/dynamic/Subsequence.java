package algorithm.dynamic;

import java.util.Arrays;

public class Subsequence {
    private static int[] getLargestIncreasingSubsequence(int[] arr){
        int n = arr.length;
        int subseq[] = new int[n];
        int trace[] = new int[n];

        Arrays.fill(subseq, 1);
        Arrays.fill(trace, -1);

        int max= subseq[0];
        int mIndex = 0;

        for(int i = 1; i<n; i++){
            for(int j = 0;j < i;j++){
                if(arr[i] > arr[j] && subseq[j]+1 > subseq[i]){
                    subseq[i] = subseq[j]+1;
                    trace[i] = j;

                    if(subseq[i] > max){
                        max = subseq[i];
                        mIndex = i;
                    }
                }
            }
        }

        int[] res = new int[max];

        for (int i = max-1; i >= 0 && mIndex >= 0 ; i--) {
            res[i] = arr[mIndex];
            mIndex = trace[mIndex];
        }

        return res;
    }

    public static void main(String[] args) {
        int arr[] = {1,3,2,4,6};

        System.out.println(Arrays.toString(getLargestIncreasingSubsequence(arr)));
    }
}
