package algorithm.graph;

import java.util.*;

class Edge{
    int src;
    int dest;

    public Edge(int src, int dest) {
        this.src = src;
        this.dest = dest;
    }
}

class Graph{
    HashMap<Integer, ArrayList<Integer>> adjList;
    public Graph(List<Edge> edges){
        adjList = new HashMap<>();

        for(Edge edge: edges){
            adjList.computeIfAbsent(edge.src, key -> new ArrayList<>());
            adjList.get(edge.src).add(edge.dest);
        }
    }
}

public class BFS {
    private static void bfsSearch(Graph graph, int src){
        HashMap<Integer, Boolean> discovered = new HashMap<>();
        ArrayDeque<Integer> queue = new ArrayDeque<>();

        queue.addLast(src);
        discovered.put(src, true);
        System.out.println(src);

        while (!queue.isEmpty()){
            int tempSrc = queue.removeFirst();
            ArrayList<Integer> child = graph.adjList.get(tempSrc);

            for(int i =0; child != null && i < child.size(); i++ ){
                int vertex = child.get(i);

                if(discovered.get(vertex)==null){
                    discovered.put(vertex, true);
                    System.out.println(vertex+" ");
                    queue.addLast(vertex);
                }
            }
        }
    }

    private static void dfsSearch(Graph graph, int src){
        HashMap<Integer, Boolean> discovered = new HashMap<>();
        ArrayDeque<Integer> queue = new ArrayDeque<>();

        queue.addLast(src);
        discovered.put(src, true);
        System.out.println(src);

        while (!queue.isEmpty()){
            int tempSrc = queue.removeFirst();
            ArrayList<Integer> child = graph.adjList.get(tempSrc);

            for(int i =0; child != null && i < child.size(); i++ ){
                int vertex = child.get(i);

                if(discovered.get(vertex)==null){
                    discovered.put(vertex, true);
                    System.out.println(vertex+" ");
                    queue.addFirst(vertex);
                }
            }
        }
    }

    public static void main(String[] args) {
        // vector of algorithm.graph edges as per above diagram
        List<Edge> edges = Arrays.asList(
                new Edge(1, 2), new Edge(1, 3), new Edge(1, 4),
                new Edge(2, 5), new Edge(2, 6), new Edge(5, 9),
                new Edge(5, 10), new Edge(4, 7), new Edge(4, 8),
                new Edge(7, 11), new Edge(7, 12)
                // vertex 0, 13 and 14 are single nodes
        );

        Graph graph = new Graph(edges);

        dfsSearch(graph, 1);
    }
}
