package algorithm.sort;

import java.util.Arrays;

class SelectionSort {
    static void selectionSort(int[] arr){
        for (int i = 0; i < arr.length - 1; i++) {
            int index = i;
            for (int j = i + 1; j < arr.length; j++){
                if (arr[j] < arr[index]){
                    index = j;
                }
            }
            int temp = arr[index];
            arr[index] = arr[i];
            arr[i] = temp;
        }
    }

    public static void main(String a[]){
        int[] arr = {9,14,3,2,43,11,58,22};

        System.out.print("Before Selection Sort: ");
        System.out.println(Arrays.toString(arr));

        selectionSort(arr);

        System.out.print("After Selection Sort: ");
        System.out.println(Arrays.toString(arr));
    }
}
