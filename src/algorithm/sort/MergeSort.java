package algorithm.sort;

import java.util.Arrays;

public class MergeSort {
    static void mergeSort(int a[], int begin, int end){
        if(begin<end){
            int mid=(begin+end)/2;
            mergeSort(a,begin,mid);
            mergeSort(a,mid+1,end);
            merge(a,begin,mid,end);
        }
    }
    static void merge(int a[], int begin, int mid, int end){
        int left_size = mid-begin+1;
        int right_size = end-mid;

        int left[] = new int[left_size+1];
        int right[] = new int[right_size+1];

        for (int i = 0; i < left.length-1; i++) {
            left[i]=a[begin+i];
        }
        for (int j = 0; j < right.length-1; j++) {
            right[j]=a[mid+1+j];
        }

        //assigning infinity to the last index of the array
        left[left_size]=Integer.MAX_VALUE;
        right[right_size]=Integer.MAX_VALUE;

        int i=0,j=0,k=0;
        for (k = begin; k<=end; k++) {
            if(left[i]<right[j]){
                a[k]=left[i];
                i++;
            }else{
                a[k]=right[j];
                j++;
            }
        }
    }

    public static void main(String a[]){
        int[] arr = {9,14,3,2,43,11,58,22};

        System.out.print("Before Selection Sort: ");
        System.out.println(Arrays.toString(arr));

        mergeSort(arr,0, arr.length-1);

        System.out.print("After Selection Sort: ");
        System.out.println(Arrays.toString(arr));
    }
}



