package algorithm.sort;

import java.util.Arrays;

public class QuickSort {
    static int partition(int a[], int begin, int end){
        int i=begin-1;
        int j=begin;
        int x = a[end];
        for (; j < end; j++) {
            if(x>a[j]){
                i++;
                int temp = a[i];
                a[i]=a[j];
                a[j]=temp;
            }
        }
        i++;
        int temp = a[i];
        a[i]=a[j];
        a[j]=temp;
        return i;
    }

    static void quickSort(int a[], int begin, int end){

        if(begin<end){
            int pivot = partition(a,begin,end);
            quickSort(a,begin,pivot-1);
            quickSort(a,pivot+1,end);
        }
    }

    public static void main(String a[]){
        int[] arr = {9,14,3,2,43,11,58,22};

        System.out.print("Before Selection Sort: ");
        System.out.println(Arrays.toString(arr));

        quickSort(arr,0, arr.length-1);

        System.out.print("After Selection Sort: ");
        System.out.println(Arrays.toString(arr));
    }


}

