package algorithm.coin;

import java.util.HashMap;
import java.util.Scanner;

public class CoinChange {

    static HashMap<Integer, Integer> count = new HashMap<>();

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.print("Please input the amount: ");
        int amount = input.nextInt();

        System.out.print("Please input the array length: ");
        int length = input.nextInt();
        int coinArray[] = new int[length];

        System.out.print("Please insert "+length+" data in descending order: ");
        for(int i=0;i<length;i++) coinArray[i] = input.nextInt();

        System.out.println("Coin Needed " +coinCount(coinArray, amount, 0));

        System.out.println("Amount of each coing " +count.toString());
    }

    static int cointCountDynamic(int coin[], int amount){
        int table[][] = new int[coin.length][amount+1];

        for(int i = 0; i<coin.length; i++){
            table[i][0] = 0;
        }

        for(int i= 0; i< amount+1; i++){
            table[0][i] = i/coin[0];
        }

        int i= 0;
        int j = 0;

        for(i = 1; i< coin.length; i++){
            for(j = 1; j < amount+1; j++){
                if(coin[i] > j){
                    table[i][j] = table[i-1][j];
                }
                else {
                    table[i][j] = Math.min(table[i][j-coin[i]]+1, table[i-1][j]);
                }
            }

        }

        j--;
        i--;

        while(i>=0 && j> 0){
            if(i>0 && table[i][j] == table[i-1][j]){
                i--;
                continue;
            }
            System.out.println(coin[i]);
            j = j - coin[i];
        }

        return table[coin.length-1][amount];
    }

    static int coinCount(int coin[], int m, int index){
        if(index>= coin.length) return 0;

        int temp = m/coin[index];

        if(temp > 0) count.put(coin[index], m/coin[index]);

        return temp+coinCount(coin,m%coin[index], ++index);
    }

}
